<?php
/**
 * @author: Jackson Tong <jackson@snapfrozen.com.au> 
 * Date: 16/03/2015
 * Time: 11:29
 */

class OrderController extends BoxomaticController
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout='//layouts/column1';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('setDelivered', 'setApproved', 'processOrder', 'refund'),
                'roles'=>array('Admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    /**
     * @param $id integer order id
     */
    public function actionProcessOrder($id)
    {
        $Order = $this->loadModel($id);
        $User = $Order->User;

        /**
         * php mysql bug when reading float number
         * @link http://stackoverflow.com/questions/10073151/reading-a-float-from-mysql
         */
        if (number_format($User->balance - $Order->total,2) >= SnapUtil::config('boxomatic/minimumCredit'))
        {
            $Payment = new UserPayment();
            $Payment->payment_value = -1 * ($Order->total); //make price a negative value for payment table
            $Payment->payment_type = 'DEBIT';
            $Payment->payment_date = new CDbExpression('NOW()');
            $Payment->user_id = $Order->user_id;
            $Payment->staff_id = Yii::app()->user->id;

            $note = 'Order for ' . $Order->DeliveryDate->date . ' : ' . SnapFormat::currency($Order->extras_total);

            $Payment->payment_note = $note;
            $Payment->save();
            $Order->status = Order::STATUS_APPROVED;

            if ($Order->save())
            {
                //if order approved then process customer boxes
                foreach ($Order->UserBoxes as $CustBox) {
                    $CustBox->status = UserBox::STATUS_APPROVED;
                    $CustBox->save();
                } // end foreach
            }
            Yii::app()->user->setFlash('success', "Customer Order has been set to Approved.");
        } else
            Yii::app()->user->setFlash('danger', 'Not enough credit.');

        $this->redirect(array('boxItem/userBoxes', 'date' => $Order->delivery_date_id));
    }

    /**
     * @param $id integer Order id
     * @throws CDbException
     * @throws CHttpException
     */
    public function actionRefund($id)
    {
        $model = $this->loadModel($id);
        $model->status = Order::STATUS_REFUNDED;
        if ($model->save())
        {
            $Payment = new UserPayment();
            $Payment->payment_value = 1 * $model->total;
            $Payment->payment_type = 'CREDIT';
            $Payment->payment_date = new CDbExpression('NOW()');
            $Payment->user_id = $model->user_id;
            $Payment->staff_id = Yii::app()->user->id;

            $note = 'REFUND FOR: 1 x Order ' . $model->DeliveryDate->date ;

            $Payment->payment_note = $note;

            if ($Payment->save())
            {
                // Delete user boxes
                foreach ($model->UserBoxes as $UserBox) {
                    $UserBox->status = UserBox::STATUS_REFUNDED;
                    $UserBox->save();
                }
                Yii::app()->user->setFlash('success', "Customer Order has been excluded for delivery and refunded.");
            }
        }

        $this->redirect(array('boxItem/userBoxes', 'date' => $model->delivery_date_id));
    }

    /**
     * @param $id integer Order id
     * @throws CHttpException
     */
    public function actionSetApproved($id)
    {
        $model = $this->loadModel($id);
        $model->status = Order::STATUS_APPROVED;

        if ($model->save())
        {
            foreach ($model->UserBoxes as $UserBox) {
                $UserBox->status = UserBox::STATUS_APPROVED;
                $UserBox->save();
            }
            Yii::app()->user->setFlash('success', "Customer Order has been set to Approved.");
        }

        $this->redirect(array('boxItem/userBoxes', 'date' => $model->delivery_date_id));
    }

    /**
     * @param $id integer Order id
     * @throws CHttpException
     */
    public function actionSetDelivered($id)
    {
        $model = $this->loadModel($id);
        $model->status = Order::STATUS_DELIVERED;

        if ($model->save())
        {
            foreach ($model->UserBoxes as $UserBox) {
                $UserBox->status = UserBox::STATUS_DELIVERED;
                $UserBox->save();
            }
            Yii::app()->user->setFlash('success', "Customer Order has been set to Delivered.");
        }

        $this->redirect(array('boxItem/userBoxes', 'date' => $model->delivery_date_id));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Order the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=Order::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

}
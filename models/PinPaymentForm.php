<?php
/**
 * Created by PhpStorm.
 * User: toantv
 * Date: 24/10/2014
 * Time: 13:53
 */

class PinPaymentForm extends CFormModel
{
    public $name;
    public $email;
    public $number;
    public $description;
    public $expiry_month;
    public $expiry_year;
    public $address_line1;
    public $address_line2;
    public $address_city;
    public $address_state;
    public $address_postcode;
    public $address_country;
    public $cvc;

    public function rules() {
        return array(
            array('name, number, cvc, expiry_month, expiry_year', 'required'),
            array('email', 'email'),
            array('cvc, address_postcode', 'numerical')
        );
    }

    /**
     * Send data to pin.net.au
     */
    public function pinPayMent()
    {
        $pin = new Pin();
        $Customer = BoxomaticUser::model()->findByPk(Yii::app()->user->id);
        $pin->charges_parameters = array(
            'amount' => str_replace('.', '',$_POST['amount']),
            'currency' => 'AUD',
            'description' => 'Boxomatic Charge',
            'email' => $this->email,
            'ip_address' => Yii::app()->request->userHostAddress,
            'card' => array(
                'number' => $this->number,
                'expiry_month' => $this->expiry_month,
                'expiry_year' => $this->expiry_year,
                'cvc' => $this->cvc,
                'name' => $this->name,
                'address_line1' => $Customer->user_address,
                'address_line2' => $Customer->user_address2,
                'address_city' => $Customer->user_suburb,
                'address_postcode' => $Customer->user_postcode,
                'address_state' => $Customer->user_state,
                'address_country' => SnapUtil::config('boxomatic/Country')
            )
        );
        return json_decode($pin->test(),true);
    }

    public static function float($value)
    {
        return number_format($value, '2', '.', '');
    }
}
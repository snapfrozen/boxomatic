<?php
/**
 * Created by PhpStorm.
 * User: toantv
 * Date: 5/7/15
 * Time: 10:54 AM
 */
?>
Hi <?php echo $Customer->first_name ?>, A reminder to order your Food Box for next <?php echo SnapFormat::date($DeliveryDate->date,'short') ?>. To place your order, please visit https://www.bellofoodbox.com.au/. Thanks, Bello Food Box Team.

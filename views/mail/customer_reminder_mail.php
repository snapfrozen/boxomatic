<?php
/**
 * Created by PhpStorm.
 * User: toantv
 * Date: 5/7/15
 * Time: 10:54 AM
 */
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Bello Food Box Reminder</title>
</head>
<body style="margin:0;">
<p>Hi <?php echo $Customer->first_name ?>,</p>

<p>Just a friendly reminder that we haven't yet received your Bello Food Box order for next <?php echo SnapFormat::date($DeliveryDate->date,'full') ?>.</p>

<p>To place your order, please click <a href="https://www.bellofoodbox.com.au">here</a></p>

<p>With Thanks,</p>

<p>The Bello Food Box Team</p>
</body>
</html>

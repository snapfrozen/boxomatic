<?php
$this->breadcrumbs=array(
	'Box-O-Matic'=>array('/snapcms/boxomatic/index'),
	'Delivery Dates',
);
$this->page_heading = 'Delivery Dates';

Yii::app()->clientScript->registerScript('initPageSize',<<<EOD
	$('#content').on('change', '.change-pageSize', function() {
		$.fn.yiiGridView.update('delivery-date-grid',{ data:{ pageSize: $(this).val() }})
	});
EOD
    ,CClientScript::POS_READY);
?>

<?php
$this->beginWidget('bootstrap.widgets.BsPanel', array(
	'title'=>'&nbsp;',
));
$pageSize = Yii::app()->user->getState('pageSize', 50);
?>
	<?php $this->widget('backend.widgets.SnapGridView', array(
		'id'=>'delivery-date-grid',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
        'summaryText' => 'Displaying {start}-{end} of {count} result(s). ' .
        CHtml::dropDownList(
            'pageSize', $pageSize, array(5 => 5, 10 => 10, 20 => 20, 50 => 50, 100 => 100), array('class' => 'change-pageSize')) .
        ' rows per page',
		'columns'=>array(
			'date',
			'notes',
			array(
				'name'=>'disabled',
				'type'=>'boolean',
				'filter'=>array(1=>'Yes',0=>'No')
			),
			array(
				'class'=>'bootstrap.widgets.BsButtonColumn',
				'template'=>'{update}{delete}'
			),
		),
	)); ?>
<?php $this->endWidget(); ?>


<?php
$this->breadcrumbs=array(
	'Box-O-Matic'=>array('/snapcms/boxomatic/index'),
	'Suppliers'=>array('supplier/admin'),
	'Purchases',
);
$this->menu=array(
	//array('icon' => 'glyphicon glyphicon-plus-sign', 'label'=>'Create Purchase', 'url'=>array('create')),
	array('icon' => 'glyphicon glyphicon-stats', 'label'=>'Reports', 'url'=>array('report')),
);
$this->page_heading = 'Purchases';
Yii::app()->clientScript->registerScript('initPageSize',<<<EOD
	$('#content').on('change', '.change-pageSize', function() {
		$.fn.yiiGridView.update('supplier-purchase-grid',{ data:{ pageSize: $(this).val() }})
	});
EOD
    ,CClientScript::POS_READY);
?>
<?php
$this->beginWidget('bootstrap.widgets.BsPanel', array(
	'title'=>'&nbsp;',
));
$pageSize=Yii::app()->user->getState('pageSize',50);
?>
<?php $this->widget('backend.widgets.SnapGridView', array(
	'id'=>'supplier-purchase-grid',
	'cssFile' => '', 
	'dataProvider'=>$model->search(),
	'filter'=>$model,
    'summaryText'=>'Displaying {start}-{end} of {count} result(s). ' .
        CHtml::dropDownList(
            'pageSize',
            $pageSize,
            array(5=>5,10=>10,20=>20,50=>50,100=>100),
            array('class'=>'change-pageSize')) .
        ' rows per page',
	'columns'=>array(
		'id',
        /*
		array(
			'name' => 'supplier_cert_search',
			'type'=>'raw',
			'value' => 'CHtml::value($data,"supplierProduct.Supplier.certification_status")',
		),
		array(
			'name' => 'item_name_search',
			'type'=>'raw',
			'value' => 'CHtml::value($data,"supplierProduct.name_with_unit")',
		),
		array(
			'name' => 'supplier_name_search',
			'type'=>'raw',
			'value' => 'CHtml::value($data,"supplierProduct.Supplier.name")',
		),
         */
        'Supplier.name',
		'delivery_date_formatted',
        array(
            'name' => 'total',
            'value' => 'SnapFormat::currency($data->total)',
        ),
		//'order_notes',
		/*
		'delivered_quantity',
		'final_price',
		'delivery_notes',
		*/
		array(
			'class'=>'bootstrap.widgets.BsButtonColumn',
			'template'=>'{view}{update}{duplicate}{delete}',
			'buttons'=>array(
				'duplicate' => array
				(
					'label' => '<i class="glyphicon glyphicon-keys"></i>',
					'options' => array('title'=>'Duplicate'),
					'url'=> 'array("supplierPurchase/duplicate","id"=>$data->id)',
				),
			)
		),
	),
)); ?>
<?php $this->endWidget(); ?>
<?php
$this->breadcrumbs=array(
	'Box-O-Matic'=>array('/snapcms/boxomatic/index'),
	'Customers'=>array('user/customers'),
	'Locations',
);
$this->page_heading = 'Locations';
$this->menu=array(
	array('icon' => 'glyphicon glyphicon-plus-sign', 'label'=>'Create Location', 'url'=>array('create')),
);
?>
<?php
$this->beginWidget('bootstrap.widgets.BsPanel', array(
	'title'=>'&nbsp;',
));
Yii::app()->clientScript->registerScript('initPageSize',<<<EOD
	$('#content').on('change', '.change-pageSize', function() {
		$.fn.yiiGridView.update('location-grid',{ data:{ pageSize: $(this).val() }})
	});
EOD
    ,CClientScript::POS_READY);

$pageSize=Yii::app()->user->getState('pageSize',50);
?>
<?php $this->widget('backend.widgets.SnapGridView', array(
	'id'=>'location-grid',
	'cssFile' => '',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
    'summaryText'=>'Displaying {start}-{end} of {count} result(s). ' .
    CHtml::dropDownList(
        'pageSize',
        $pageSize,
        array(5=>5,10=>10,20=>20,50=>50,100=>100),
        array('class'=>'change-pageSize')) .
    ' rows per page',
	'columns'=>array(
		'location_name',
		'location_delivery_value:currency',
		array(
			'name'=>'is_pickup',
			'type'=>'boolean',
			'filter'=>array(1=>'Yes',0=>'No')
		),
		array(
			'class'=>'bootstrap.widgets.BsButtonColumn',
		),
	),
)); ?>
<?php $this->endWidget(); ?>




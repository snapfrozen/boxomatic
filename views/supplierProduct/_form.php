<?php
/**
 * @var $this SupplierProductController
 * @var $form SnapActiveForm
 * @var $model SupplierProduct
 */
?>
<div class="form">
    <?php
    $form = $this->beginWidget('application.widgets.SnapActiveForm', array(
        'id' => 'supplier-item-form',
        'enableAjaxValidation' => false,
        'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
        'htmlOptions' => array('enctype' => 'multipart/form-data', 'class' => 'row')
    ));
    ?>
    <?php if ($form->errorSummary($model)): ?>
        <div class="col-md-12">
            <div data-alert class="alert-box alert">
                <?php echo $form->errorSummary($model); ?>
            </div>
        </div>
    <?php endif; ?>
    <div class="col-lg-9 clearfix">

        <?php echo $form->textFieldControlGroup($model, 'name', array('size' => 45, 'maxlength' => 45)); ?>

        <?php echo $form->dropDownListControlGroup($model, 'supplier_id', CHtml::listData(Supplier::model()->findAll(), 'id', 'name'), array('class' => 'chosen')); ?>
        <?php echo $form->dropDownListControlGroup($model, 'packing_station_id', CHtml::listData(PackingStation::model()->findAll(), 'id', 'name'), array('class' => 'chosen')); ?>

        <?php echo $form->textFieldControlGroup($model, 'value', array('size' => 7, 'maxlength' => 7)); ?>
        <?php echo $form->dropDownListControlGroup($model, 'unit', $model->getUnitList()); ?>

        <?php echo $form->dropDownListControlGroup($model, 'available_from', $model->getMonthList()); ?>
        <?php echo $form->dropDownListControlGroup($model, 'available_to', $model->getMonthList()); ?>
        <?php echo $form->textFieldControlGroup($model, 'item_sales_price'); ?>
        <?php echo $form->textFieldControlGroup($model, 'display_order'); ?>
        <?php echo $form->checkBoxControlGroup($model, 'available_in_shop'); ?>

        <?php echo $form->imageField($model, 'image'); ?>
        <?php echo $form->textAreaControlGroup($model, 'description'); ?>

        <?php
        /**
         * set default date
         */
        if ($model->isNewRecord) {
            $model->customer_available_from = date('Y/m/d');
            $model->customer_available_to = date('Y/m/d', strtotime("+7 day"));
        }
        ?>

        <?php echo $form->dateFieldControlGroup($model, 'customer_available_from', array(), array('yearRange' => date('Y') . ':2050')); ?>
        <?php echo $form->dateFieldControlGroup($model, 'customer_available_to', array(), array('yearRange' => date('Y') . ':2050')); ?>

        <div class="row">
            <div class="col-md-offset-2 col-md-10">
                <h3>Categories</h3>
                <ul class="categories">
                    <?php echo Category::model()->getCategoryTreeForm(SnapUtil::config('boxomatic/supplier_product_root_id'), $model); ?>
                </ul>
            </div>
        </div>

    </div>
    <div id="sidebar" class="col-lg-3">
        <?php
        $this->beginWidget('bootstrap.widgets.BsPanel', array(
            'title' => 'Menu',
            'contentCssClass' => '',
            'htmlOptions' => array(
                'class' => 'panel sticky',
            ),
            'type' => BsHtml::PANEL_TYPE_PRIMARY,
        ));
        ?>		
        <div class="btn-group btn-group-vertical">
            <?php
            if ($model->isNewRecord) {
                echo BsHtml::submitButton(BsHtml::icon(BsHtml::GLYPHICON_FLOPPY_SAVED) . 'Save and add another product', array('value' => 'create', 'name' => 'option_save'));
                echo BsHtml::submitButton(BsHtml::icon(BsHtml::GLYPHICON_LINK) . 'Save and go to product list', array('value' => 'admin', 'name' => 'option_save'));
            } else {
                echo BsHtml::submitButton(BsHtml::icon(BsHtml::GLYPHICON_THUMBS_UP) . ' Save', array('value' => 'view', 'name' => 'option_save'));
            }

            $this->widget('application.widgets.SnapMenu', array(
                'items' => $this->menu,
                'htmlOptions' => array('class' => 'nav nav-stacked'),
            ));
            ?>			
        </div>
        <?php $this->endWidget(); ?>	
    </div>

    <?php $this->endWidget(); ?>
</div>

<?php
/**
 * @var $this BoxItemController
 * @var $SelectedDeliveryDate DeliveryDate
 * @var $DeliveryDates DeliveryDate
 * @var $UserBoxes UserBox
 * @var $OrdersWithExtras Order
 * @var $Order Order
 */
$baseUrl = $this->createFrontendUrl('/') . '/themes/boxomatic/admin';
$cs = Yii::app()->clientScript;
$cs->registerCssFile($baseUrl . '/css/ui-lightness/jquery-ui.min.css');

$cs->registerCoreScript('jquery.ui');
$cs->registerScriptFile($baseUrl . '/js/ui.touch-punch.min.js', CClientScript::POS_END);
$cs->registerScriptFile($baseUrl . '/js/boxitem/userBoxes.js', CClientScript::POS_END);

$this->breadcrumbs = array(
    'Box-O-Matic' => array('/snapcms/boxomatic/index'),
    'Customers' => array('user/customers'),
    'Orders',
);
//$this->page_heading = 'Orders';


if ($SelectedDeliveryDate):
//	$this->page_heading_subtext = 'for '.SnapFormat::date($SelectedDeliveryDate->date);
    $this->menu = array(
        array('icon' => 'glyphicon-cog', 'label' => 'Process Customers', 'url' => array('deliveryDate/processCustomers', 'date' => $SelectedDeliveryDate->id), 'linkOptions' => array('confirm' => 'Are you sure you want to process all customers?')),
        array('icon' => 'glyphicon-ok', 'label' => 'Pack Boxes', 'url' => array('boxItem/create', 'date' => $SelectedDeliveryDate->id)),
    );
else:
    $this->menu = array(
        array('icon' => 'glyphicon-refresh', 'label' => 'Process Customers', 'url' => 'javascript:void(0)', 'linkOptions' => array('class' => 'text-muted', 'confirm' => 'Are you sure you want to process all customers?')),
        array('icon' => 'glyphicon-ok', 'label' => 'Pack Boxes', 'url' => 'javascript:void(0)', 'linkOptions' => array('class' => 'text-muted')),
    );
endif;

Yii::app()->clientScript->registerScript('initPageSize', <<<EOD
	$('#content').on('change', '.change-pageSize', function() {
		$.fn.yiiGridView.update('customer-extras-grid',{ data:{ pageSize: $(this).val() }})
	});
	$('.search-form form').submit(function(){
		$.fn.yiiGridView.update('customer-extras-grid', {
			data: $(this).serialize()
		});
		return false;
	});
EOD
, CClientScript::POS_READY);
?>
<div class="page-header">
    <div class="row">
        <div class="col-sm-1"><h1>Orders</h1></div>
        <div class="col-sm-2">
            <input type="text" id="find-by-date" value="<?php echo date("d-m-Y", strtotime($SelectedDeliveryDate->date)) ?>" class="form-control" style="background-color:#f8f8f8;border:none;border-bottom: 1px solid"/>
        </div>
    </div>
    <script type="text/javascript">
        var curUrl = "<?php echo $this->createUrl('boxItem/userBoxes'); ?>";
        var selectedDate =<?php echo $SelectedDeliveryDate ? "'$SelectedDeliveryDate->date'" : 'null' ?>;
        var availableDates =<?php echo json_encode(SnapUtil::makeArray($DeliveryDates)) ?>;
    </script>
</div>
<div class="row">
    <div id="customerList" class="col-md-9">
        <?php
            $dataProvider = $SelectedDeliveryDate ? $UserBoxes->boxSearch($SelectedDeliveryDate->id) : $UserBoxes->boxSearch(-1);
            $pageSize = Yii::app()->user->getState('pageSize', 50);
        ?>

        <?php
        $this->beginWidget('bootstrap.widgets.BsPanel', array(
            'title' => 'Order Items',
        ));
        ?>
        <?php
        $this->widget('bootstrap.widgets.BsGridView', array(
            'id' => 'customer-extras-grid',
            'cssFile' => '',
            'dataProvider' => $OrdersWithExtras,
            'filter' => $Order,
            'summaryText' => 'Displaying {start}-{end} of {count} result(s). ' .
            CHtml::dropDownList(
                    'pageSize', $pageSize, array(5 => 5, 10 => 10, 20 => 20, 50 => 50, 100 => 100), array('class' => 'change-pageSize')) .
            ' rows per page',
            'columns' => array(
                'customer_user_id',
                array(
                    'name' => 'search_full_name',
                    'type' => 'raw',
                    'value' => 'CHtml::link(CHtml::value($data,"User.full_name"),array("user/view","id"=>$data->User->id))',
                ),
                array(
                    'name' => 'User.balance',
                    'value' => 'SnapFormat::currency($data->User->balance)',
                ),
                array(
                    'name' => 'extras_total',
                    'value' => 'SnapFormat::currency($data->total)',
                    'filter' => false,
                ),
                array(
                    'name' => 'extras_item_names',
                    'type' => 'raw',
                    'value' => 'BsHtml::tooltip(strlen($data->extras_item_names) > 10 ? substr($data->extras_item_names,0,15)."..." : $data->extras_item_names, "javascript:void(0)", $data->extras_item_names)',
                ),
                array(
                    'header' => 'Boxes',
                    'type' => 'raw',
                    'value' => 'BsHtml::tooltip(strlen($data->boxesName) > 6 ? substr($data->boxesName,0,6)."..." : $data->boxesName, "javascript:void(0)", $data->boxesName)'
                ),
                array(
                    'name' => 'status',
                    'value' => '$data->status_text',
                    'filter' => Order::model()->statusOptions,
                ),
                array(
                    'class' => 'bootstrap.widgets.BsButtonColumn',
                    'header' => 'Actions',
                    'template' => '{login}{process}{cancel}{set_approved}{set_delivered}',
                    'buttons' => array(
                        'login' => array
                            (
                            'label' => '<i class="glyphicon glyphicon-user"></i>',
                            'url' => 'array("user/loginAs","id"=>$data->User->id)',
                            'options' => array('title' => 'Login As'),
                        ),
                        'process' => array
                            (
                            'label' => '<i class="glyphicon glyphicon-cog"></i>',
                            'url' => 'array("order/processOrder","id"=>$data->id)',
                            'visible' => '$data->status==Order::STATUS_DECLINED',
                            'options' => array('title' => 'Process'),
                        ),
                        'cancel' => array
                            (
                            'url' => 'array("order/refund","id"=>$data->id)',
                            'visible' => '$data->status==Order::STATUS_APPROVED',
                            'label' => '<i class="glyphicon glyphicon-ban-circle"></i>',
                            'options' => array('confirm' => 'Are you sure you want to refund this order?', 'title' => 'Cancel & Refund'),
                        ),
                        'set_approved' => array
                            (
                            'url' => 'array("order/setApproved","id"=>$data->id)',
                            'visible' => '$data->status==Order::STATUS_DELIVERED',
                            'label' => '<i class="glyphicon glyphicon-check"></i>',
                            'options' => array('confirm' => 'Are you sure you want to set this box to Approved?', 'title' => 'Set Approved'),
                        ),
                        'set_delivered' => array
                            (
                            'url' => 'array("order/setDelivered","id"=>$data->id)',
                            'visible' => '$data->status==Order::STATUS_APPROVED',
                            'label' => '<i class="glyphicon glyphicon-thumbs-up"></i>',
                            'options' => array('confirm' => 'Are you sure you want to set this box to Collected/Delivered?', 'title' => 'Set Delivered'),
                        )
                    ), // end of buttons
                ),
            ), // end of columns
        )); // end of BsGridview
        ?>
        <?php $this->endWidget(); ?>
    </div>

    <div class="col-md-3">
        <div class="sticky">
            <?php
            $this->beginWidget('bootstrap.widgets.BsPanel', array(
                'title' => 'Menu',
                'contentCssClass' => '',
                'htmlOptions' => array(
                    'class' => 'panel',
                ),
                'type' => BsHtml::PANEL_TYPE_PRIMARY,
            ));
            $this->widget('application.widgets.SnapMenu', array(
                'items' => $this->menu,
                'htmlOptions' => array('class' => 'nav nav-stacked'),
            ));
            $this->endWidget();
            ?>
        </div>
    </div>
</div>


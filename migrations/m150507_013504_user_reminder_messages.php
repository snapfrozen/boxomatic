<?php

class m150507_013504_user_reminder_messages extends CDbMigration
{
	public function up()
	{
		$this->addColumn('{{users}}', 'reminder_messages', 'TINYINT(1) DEFAULT 1');
	}

	public function down()
	{
		$this->dropColumn('{{users}}', 'reminder_messages');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
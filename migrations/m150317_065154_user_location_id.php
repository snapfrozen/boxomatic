<?php

class m150317_065154_user_location_id extends CDbMigration
{
	public function up()
	{
        $this->execute("UPDATE snap_users u
        INNER JOIN boxo_user_locations ul ON u.id = ul.user_id
        INNER JOIN boxo_locations l ON ul.location_id = l.location_id
        SET u.user_location_id = ul.customer_location_id
        WHERE u.user_location_id IS NULL");
	}

	public function down()
	{
		echo "m150317_065154_user_location_id does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
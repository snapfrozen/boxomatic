<?php

class m150313_063234_supplier_image extends CDbMigration
{
	public function up()
	{
        $this->addColumn(SnapUtil::config('boxomatic/tablePrefix') . 'suppliers', 'image', 'VARCHAR(255)');
	}

	public function down()
	{
        $this->dropColumn(SnapUtil::config('boxomatic/tablePrefix') . 'suppliers', 'image');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
<?php

class m150313_015808_supplier_profile extends CDbMigration
{
	public function up()
	{
        $this->addColumn(SnapUtil::config('boxomatic/tablePrefix') . 'suppliers', 'profile', 'TEXT');
	}

	public function down()
	{
        $this->dropColumn(SnapUtil::config('boxomatic/tablePrefix') . 'suppliers', 'profile');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}